package flow.digital.respondentpolice.utils;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeConverterUtils {
    private Context context;

    public TimeConverterUtils(Context context) {
        this.context = context;
    }

    public String covertTimeStamp(String time) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(format.parse(time));

        SimpleDateFormat convertFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm a");

        String convertTime = convertFormat.format(calendar.getTime());

        return convertTime;
    }
}
