package flow.digital.respondentpolice.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Iterator;

import flow.digital.respondentpolice.constants.Constants;

public class SharedPrefUtils {
    public SharedPrefUtils() {

    }

    //------ Write Shared Preferences
    public void putPref(HashMap<String, String> map, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) iterator.next();
            editor.putString(pair.getKey().toString().trim(), pair.getValue().toString().trim());
            iterator.remove();
        }
        editor.apply();
    }

    //--- Read Preferences -----
    public String getPref(String prefKey, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        return preferences.getString(prefKey.trim(), "");
    }

    public int getIntPref(String prefKey, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        return preferences.getInt(prefKey.trim(), -1);
    }

    public boolean getBooleanPref(String prefKey, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        return preferences.getBoolean(prefKey.trim(), false);
    }

    //--- Clear All Preferences ---
    public void clearAllPref(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    //--- Clear single preferences ---
    public void clearPref(String prefKey, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        preferences.edit().remove(prefKey).apply();
    }

    //------ Write Shared Preferences
    public void putPref(String key, String value, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putPref(String key, int value, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putPref(String key, boolean value, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
}//end of SharedPrefUtils class
