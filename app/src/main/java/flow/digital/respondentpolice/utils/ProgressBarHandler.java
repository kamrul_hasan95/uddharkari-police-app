package flow.digital.respondentpolice.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import androidx.annotation.Nullable;
import flow.digital.respondentpolice.R;


public class ProgressBarHandler {

    //private ProgressBar mProgressBar;
    //private Activity activity;
    private Dialog dialog;

    public ProgressBarHandler(Context context) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //...set cancelable false so that it's never get hidden
        dialog.setCancelable(false);
        //...that's the layout i told you will inflate later
        dialog.setContentView(R.layout.custom_loading_layout);

        //...initialize the imageView form infalted layout
        ImageView gifImageView = dialog.findViewById(R.id.custom_loading_imageView);


        //...now load that gif which we put inside the drawble folder here with the help of Glide

        Glide.with(context)
                .load(R.drawable.progress_indicator)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        gifImageView.setVisibility(View.VISIBLE);
                        return false;
                    }
                }).into(gifImageView);


    }

    public void showProgress() {
        dialog.show();

    }

    public void hideProgress() {
        dialog.dismiss();
    }
}