package flow.digital.respondentpolice.appModule.splash.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;

import flow.digital.respondentpolice.apiService.AuthWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.sendModel.FcmTokenSendModel;
import flow.digital.respondentpolice.dataModel.sendModel.LoginSendModel;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class SplashPresenter {
    private Context context;
    private SplashViewInterface viewInterface;
    private SharedPrefUtils utils;

    public SplashPresenter(Context context, SplashViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
        this.utils = new SharedPrefUtils();
    }

    public void login(String userName, String password) {
        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        LoginSendModel sendModel = new LoginSendModel();
        sendModel.username = userName;
        sendModel.password = password;

        Timber.d(sendModel.toString());

        webService.login(sendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        if (jsonObjectResponse.isSuccessful()) {
                            utils.putPref(Constants.SHARED_USERID, jsonObjectResponse.body().get("userId").getAsString(), context);
                            Timber.d(jsonObjectResponse.body().get("userId").getAsString());
                            saveFcmToken(jsonObjectResponse.headers().get("set-cookie"));
//                            viewInterface.onSuccess();
//                            saveToken(jsonObjectResponse.headers().get("set-cookie"), jsonObjectResponse.body().get("userId").getAsString());
                        } else {
                            viewInterface.onError();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveFcmToken(String sessionId) {
        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        FcmTokenSendModel sendModel = new FcmTokenSendModel();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Timber.w("getInstanceId failed" + task.getException());
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();
                Timber.d("firebase" + token);
                sendModel.userToken = token;
                utils.putPref(Constants.SHARE_TOKEN, token, context);

                webService.saveFcmToken(sessionId, sendModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<JsonObject>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Response<JsonObject> jsonObjectResponse) {
                                Timber.d(jsonObjectResponse.toString());
                                if (jsonObjectResponse.isSuccessful()) {
                                    Timber.d(jsonObjectResponse.body().toString());
                                    viewInterface.onSuccess();
                                    utils.putPref(Constants.SHARED_SESSION, sessionId, context);
                                } else {
                                    viewInterface.onError();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
            }
        });
    }

//    private void saveToken(String sessionId, String userId){
//        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(
//                "http://104.192.5.213/getRespondentBeamToken",
//                new AuthDataGetter() {
//                    @Override
//                    public AuthData getAuthData() {
//                        // Headers and URL query params your auth endpoint needs to
//                        // request a Beams Token for a given user
//                        HashMap<String, String> headers = new HashMap<>();
//                        // for example:
//                        // headers.put("Authorization", sessionToken);
//                        headers.put("Cookie", sessionId);
//                        HashMap<String, String> queryParams = new HashMap<>();
//                        return new AuthData(
//                                headers,
//                                queryParams
//                        );
//                    }
//                }
//        );
//
//
//        PushNotifications.setUserId(userId, tokenProvider, new BeamsCallback<Void, PusherCallbackError>(){
//            @Override
//            public void onSuccess(Void... values) {
//                Timber.d("PusherBeams: "+"Successfully authenticated with Pusher Beams");
//                viewInterface.onSuccess();
//            }
//
//            @Override
//            public void onFailure(PusherCallbackError error) {
//                Timber.d("PusherBeams: "+"Pusher Beams authentication failed: " + error.getMessage());
//                viewInterface.onError();
//            }
//        });
//    }
}
