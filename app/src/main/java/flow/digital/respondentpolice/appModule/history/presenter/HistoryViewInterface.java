package flow.digital.respondentpolice.appModule.history.presenter;

import flow.digital.respondentpolice.base.MvpView;
import flow.digital.respondentpolice.dataModel.response.HistoryResponse;

public interface HistoryViewInterface extends MvpView {
    void onHistoryResponse(HistoryResponse response);
}
