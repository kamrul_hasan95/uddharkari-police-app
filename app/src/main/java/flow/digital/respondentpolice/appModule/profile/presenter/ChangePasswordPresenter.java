package flow.digital.respondentpolice.appModule.profile.presenter;

import android.content.Context;

import com.google.gson.JsonObject;

import flow.digital.respondentpolice.apiService.ProfileWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.sendModel.ChangePasswordSendModel;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class ChangePasswordPresenter {
    private Context context;
    private ChangePasswordViewInterface viewInterface;

    public ChangePasswordPresenter(Context context, ChangePasswordViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void changePassword(String cookie, String password){
        ProfileWebService webService = WebServiceFactory.createRetrofitService(ProfileWebService.class);
        Timber.d(password);
        Timber.d(cookie);

        ChangePasswordSendModel sendModel = new ChangePasswordSendModel();
        sendModel.newPassword = password;


        webService.changePassword(cookie, sendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        viewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if (jsonObjectResponse.isSuccessful()){
                            new SharedPrefUtils().putPref(Constants.SHARED_PASSWORD, password, context);
                            viewInterface.onSuccess();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        viewInterface.hideProgress();
                    }
                });
    }
}
