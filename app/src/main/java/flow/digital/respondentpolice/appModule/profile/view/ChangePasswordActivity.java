package flow.digital.respondentpolice.appModule.profile.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.profile.presenter.ChangePasswordPresenter;
import flow.digital.respondentpolice.appModule.profile.presenter.ChangePasswordViewInterface;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.utils.ProgressBarHandler;
import flow.digital.respondentpolice.utils.SharedPrefUtils;

import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordViewInterface {
    private ProgressBarHandler progressBarHandler;
    private ChangePasswordPresenter presenter;
    private SharedPrefUtils prefUtils;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tilOldPassword)
    TextInputLayout tilOldPassword;
    @BindView(R.id.tilNewPassword)
    TextInputLayout tilNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.change_password));
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }

        presenter = new ChangePasswordPresenter(this,this);
        progressBarHandler = new ProgressBarHandler(this);
        prefUtils = new SharedPrefUtils();

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private boolean validateOldPassword(){
        if (tilOldPassword.getEditText().getText().toString().trim() != null && tilOldPassword.getEditText().getText().toString().trim().length() < 12){
            tilOldPassword.setError(getResources().getString(R.string.password_null));
            tilOldPassword.setErrorEnabled(true);
            return false;
        }else if (!tilOldPassword.getEditText().getText().toString().trim().equals(prefUtils.getPref(Constants.SHARED_PASSWORD, this))){
            tilOldPassword.setError(getResources().getString(R.string.password_old_not_match));
            tilOldPassword.setErrorEnabled(true);
            return false;
        }else {
            tilOldPassword.setError("");
            tilOldPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateNewPassword(){
        if (tilNewPassword.getEditText().getText().toString().trim() != null && tilNewPassword.getEditText().getText().toString().trim().length() < 12){
            tilNewPassword.setError(getResources().getString(R.string.password_null));
            tilNewPassword.setErrorEnabled(true);
            return false;
        }else {
            tilNewPassword.setError("");
            tilNewPassword.setErrorEnabled(false);
            return true;
        }
    }

    @OnClick(R.id.btnChangePassword)
    public void changePassword(){
        if (validateOldPassword() | validateNewPassword()){
            presenter.changePassword(prefUtils.getPref(Constants.SHARED_SESSION,this), tilNewPassword.getEditText().getText().toString().trim());
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_change_password;
    }

    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }

    @Override
    public void onSuccess() {
        finish();
    }
}
