package flow.digital.respondentpolice.appModule.registration.presenter;

import flow.digital.respondentpolice.base.MvpView;

public interface RegistrationViewInterface extends MvpView {
    void onRegistrationSuccess();
    void onRegistrationFailed();
}
