package flow.digital.respondentpolice.appModule.about.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.about.presenter.AboutPresenter;
import flow.digital.respondentpolice.appModule.about.presenter.AboutViewInterface;
import flow.digital.respondentpolice.base.BaseFragment;
import flow.digital.respondentpolice.utils.ProgressBarHandler;

public class AboutFragment extends BaseFragment implements AboutViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;

    private AboutPresenter presenter;
    private ProgressBarHandler progressBarHandler;

    public AboutFragment() {
        //empty constructor for fragment
    }

    public static AboutFragment newInstance(String title) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarHandler = new ProgressBarHandler(getContext());
        presenter = new AboutPresenter(getContext(), this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_about;
    }

    @Override
    public void showProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
