package flow.digital.respondentpolice.appModule.login.view;

import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.login.presenter.LoginPresenter;
import flow.digital.respondentpolice.appModule.login.presenter.LoginViewInterface;
import flow.digital.respondentpolice.appModule.mother.MotherActivity;
import flow.digital.respondentpolice.appModule.registration.view.RegistrationActivity;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.utils.ProgressBarHandler;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends BaseActivity implements LoginViewInterface {
    @BindView(R.id.tilUsername)
    TextInputLayout tilUsername;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;

    ProgressBarHandler progressBarHandler;
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter(this,this);
        progressBarHandler = new ProgressBarHandler(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @OnClick(R.id.btnLogin)
    public void loginButtonClick(){
//        presenter.login("+8801911666777", "victim2");
        if (validateUserName() | validatePassword()){
            presenter.login(tilUsername.getEditText().getText().toString().trim(), tilPassword.getEditText().getText().toString().trim());
        }
    }

    @OnClick(R.id.btnRegistration)
    public void registrationButtonClicked(){
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    @Override
    public void onLoginSuccess() {
        finish();
        startActivity(new Intent(this, MotherActivity.class));
    }

    @Override
    public void onLoginFailed() {

    }

    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }

    private boolean validateUserName(){
        if (tilUsername.getEditText().getText().toString().trim().length() < 1){
            tilUsername.setErrorEnabled(true);
            tilUsername.setError(getResources().getString(R.string.username_null));
            return false;
        }else {
            tilUsername.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validatePassword(){
        if (tilPassword.getEditText().getText().toString().trim().length() < 1){
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getResources().getString(R.string.password_null));
            return false;
        }else {
            tilPassword.setErrorEnabled(false);
            return true;
        }
    }
}
