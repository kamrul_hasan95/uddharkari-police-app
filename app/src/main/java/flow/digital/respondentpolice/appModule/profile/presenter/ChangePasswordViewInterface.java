package flow.digital.respondentpolice.appModule.profile.presenter;

import flow.digital.respondentpolice.base.MvpView;

public interface ChangePasswordViewInterface extends MvpView {
    void onSuccess();
}
