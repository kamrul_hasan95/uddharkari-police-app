package flow.digital.respondentpolice.appModule.splash.presenter;

public interface SplashViewInterface {
    void onSuccess();
    void onError();
}
