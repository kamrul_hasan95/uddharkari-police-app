package flow.digital.respondentpolice.appModule.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.dataModel.Case;
import flow.digital.respondentpolice.utils.TimeConverterUtils;

public class AdapterRvHistory extends RecyclerView.Adapter<AdapterRvHistory.ViewHolderHistory>{
    private Context context;
    private List<Case> histories;

    public AdapterRvHistory(Context context, List<Case> histories) {
        this.context = context;
        this.histories = histories;
    }

    class ViewHolderHistory extends RecyclerView.ViewHolder{
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvPlace)
        TextView tvPlace;
        public ViewHolderHistory(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolderHistory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_help_history, parent, false);
        return new ViewHolderHistory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderHistory holder, int position) {
        holder.tvPlace.setText(context.getResources().getString(R.string.place)+" "+histories.get(position).crimeLocation);
        try {
            holder.tvTime.setText(context.getResources().getString(R.string.time)+" "+new TimeConverterUtils(context).covertTimeStamp(histories.get(position).filedAt));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.tvTime.setText(context.getResources().getString(R.string.time)+" "+histories.get(position).filedAt);
        }
    }

    @Override
    public int getItemCount() {
        return histories.size();
    }
}
