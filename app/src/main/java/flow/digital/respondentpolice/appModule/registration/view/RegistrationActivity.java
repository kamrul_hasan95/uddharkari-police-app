package flow.digital.respondentpolice.appModule.registration.view;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.registration.presenter.RegistrationPresenter;
import flow.digital.respondentpolice.appModule.registration.presenter.RegistrationViewInterface;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.utils.ProgressBarHandler;
import timber.log.Timber;

public class RegistrationActivity extends BaseActivity implements RegistrationViewInterface {
    @BindView(R.id.tilFirstName)
    TextInputLayout tilFirstName;
    @BindView(R.id.tilLastName)
    TextInputLayout tilLastName;
    @BindView(R.id.tilAge)
    TextInputLayout tilAge;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.tilContactNumber)
    TextInputLayout tilContactNumber;
    @BindView(R.id.spnDesignation)
    Spinner spnDesignation;
    @BindView(R.id.spnGender)
    Spinner spnGender;
    @BindView(R.id.spnStations)
    Spinner spnStations;

    List<String> designationList;
    List<String> genderList;
    List<String> stationList;

    ProgressBarHandler progressBarHandler;
    RegistrationPresenter presenter;

    int selectedDesignation, selectedGender, selectedStation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new RegistrationPresenter(this, this);
        progressBarHandler = new ProgressBarHandler(this);
        initData();
    }

    private void initData(){
        designationList = Arrays.asList(getResources().getStringArray(R.array.designation_array));
        genderList = Arrays.asList(getResources().getStringArray(R.array.gender_array));
        stationList = Arrays.asList(getResources().getStringArray(R.array.thana_list));
        initView();
    }

    private void initView(){
        ArrayAdapter<String> adapterDesignation = new ArrayAdapter<String>(this, R.layout.item_spinner_text, designationList){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(16);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };

        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, R.layout.item_spinner_text, genderList){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(16);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };

        ArrayAdapter<String> adapterStation = new ArrayAdapter<String>(this, R.layout.item_spinner_text, stationList){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(16);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };

        spnDesignation.setAdapter(adapterDesignation);
        spnGender.setAdapter(adapterGender);
        spnStations.setAdapter(adapterStation);

        spnDesignation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDesignation = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGender = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnStations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedStation = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public int getLayout() {
        return R.layout.activity_registration;
    }

    @OnClick(R.id.btnRegister)
    public void registrationButtonClick(){
        if (validateFirstName() | validateLastName() | validateAge() | validateContactNumber() | validateUPassowrd() | validateDesignationSpn() | validateGenderSpn() | validateStationSpn()){
            presenter.registerUser(
                    tilFirstName.getEditText().getText().toString().trim(),
                    tilLastName.getEditText().getText().toString().trim(),
                    tilAge.getEditText().getText().toString().trim(),
                    designationList.get(selectedDesignation),
                    tilContactNumber.getEditText().getText().toString().trim(),
                    getGender(),
                    tilPassword.getEditText().getText().toString().trim(),
                    getStation()
            );
        }
    }

    @Override
    public void onRegistrationSuccess() {
        finish();
    }

    @Override
    public void onRegistrationFailed() {
        Toast.makeText(this, getResources().getString(R.string.registration_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }

    private String getGender(){
        if (selectedGender == 1){
            return "M";
        }else if (selectedGender == 2){
            return "F";
        }
        return "";
    }

    private String getStation(){
        return stationList.get(selectedStation);
    }

    private boolean validateDesignationSpn(){
        Timber.d("validateDesignation"+designationList.get(selectedDesignation));
        return selectedDesignation != 0;
    }

    private boolean validateGenderSpn(){
        Timber.d("validateGenderSpn"+genderList.get(selectedGender));
        return selectedDesignation != 0;
    }

    private boolean validateStationSpn(){
        Timber.d("validateStation: "+stationList.get(selectedStation));
        return selectedStation != 0;
    }

    private boolean validateFirstName(){
        if (tilFirstName.getEditText().getText().toString().trim().length() < 3){
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getResources().getString(R.string.firstname_null));
            return false;
        }else {
            tilFirstName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateLastName(){
        if (tilLastName.getEditText().getText().toString().trim().length() < 3){
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getResources().getString(R.string.lastname_null));
            return false;
        }else {
            tilLastName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateAge(){
        if (tilAge.getEditText().getText().toString().trim().length() < 1 && Integer.parseInt(tilAge.getEditText().getText().toString().trim()) > 10){
            tilAge.setErrorEnabled(true);
            tilAge.setError(getResources().getString(R.string.agename_null));
            return false;
        }else {
            tilAge.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateContactNumber(){
        if (tilContactNumber.getEditText().getText().toString().trim().length() < 11){
            tilContactNumber.setErrorEnabled(true);
            tilContactNumber.setError(getResources().getString(R.string.contact_null));
            return false;
        }else {
            tilContactNumber.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateUPassowrd(){
        if (tilPassword.getEditText().getText().toString().trim().length() < 4){
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getResources().getString(R.string.password_null));
            return false;
        }else {
            tilPassword.setErrorEnabled(false);
            return true;
        }
    }
}
