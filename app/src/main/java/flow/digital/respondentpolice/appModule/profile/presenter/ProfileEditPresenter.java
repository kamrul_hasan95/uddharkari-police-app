package flow.digital.respondentpolice.appModule.profile.presenter;

import android.content.Context;

import com.google.gson.JsonObject;

import flow.digital.respondentpolice.apiService.ProfileWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.sendModel.ProfileUpdateSendModel;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class ProfileEditPresenter {
    Context context;
    ProfileEditViewInterface viewInterface;

    public ProfileEditPresenter(Context context, ProfileEditViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void udpateProfile(ProfileUpdateSendModel updateJson){
        ProfileWebService webService = WebServiceFactory.createRetrofitService(ProfileWebService.class);
        String sessionId = new SharedPrefUtils().getPref(Constants.SHARED_SESSION, context);
        String updates = "\"{ \"designation\" : \""+updateJson.updates.designation+"\" }";

        webService.updateProfile(sessionId, updates)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        viewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if (jsonObjectResponse.isSuccessful()){
                            viewInterface.onProfileUpdateSuccess();
                        }else {
                            viewInterface.onProfileUpdateFailed();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        viewInterface.hideProgress();
                    }
                });
    }
}
