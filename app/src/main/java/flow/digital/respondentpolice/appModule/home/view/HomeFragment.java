package flow.digital.respondentpolice.appModule.home.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.home.presenter.HomePresenter;
import flow.digital.respondentpolice.appModule.home.presenter.HomeViewInterface;
import flow.digital.respondentpolice.base.BaseFragment;
import flow.digital.respondentpolice.utils.ProgressBarHandler;

public class HomeFragment extends BaseFragment implements HomeViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;

    HomePresenter presenter;
    ProgressBarHandler progressBarHandler;

    public HomeFragment() {
        //empty constructor for fragment
    }

    public static HomeFragment newInstance(String title) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarHandler = new ProgressBarHandler(getContext());
        presenter = new HomePresenter(getContext(), this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void showProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
