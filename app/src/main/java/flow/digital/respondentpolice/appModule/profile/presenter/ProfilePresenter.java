package flow.digital.respondentpolice.appModule.profile.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import flow.digital.respondentpolice.apiService.AuthWebService;
import flow.digital.respondentpolice.apiService.ProfileWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.appModule.mother.MotherActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.response.ProfileResponse;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import timber.log.Timber;

public class ProfilePresenter {
    ProfileViewInterface profileViewInterface;
    Context context;

    public ProfilePresenter(Context context, ProfileViewInterface profileViewInterface) {
        this.profileViewInterface = profileViewInterface;
        this.context = context;
    }



    public void getProfile(String sessionId){
        ProfileWebService webService = WebServiceFactory.createRetrofitService(ProfileWebService.class);

        webService.getProfile(sessionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ProfileResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        profileViewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<ProfileResponse> profileResponseResponse) {
                        Timber.d(profileResponseResponse.toString());
                        if (profileResponseResponse.isSuccessful()){
                            Timber.d(profileResponseResponse.body().toString());
                            profileViewInterface.onProfileFetched(profileResponseResponse.body().profile);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        profileViewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        profileViewInterface.hideProgress();
                    }
                });
    }

    public void logout(){
        String sessionId = new SharedPrefUtils().getPref(Constants.SHARED_SESSION, context);

        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        webService.logout(sessionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        profileViewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if (jsonObjectResponse.isSuccessful()){

                            profileViewInterface.onLogoutSuccess();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        profileViewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        profileViewInterface.hideProgress();
                    }
                });
    }

    public void getPicture(MotherActivity activity){
        TedBottomPicker.with(activity)
                .show(new TedBottomSheetDialogFragment.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        if (uri != null) {
                            File file = saveBitmapToFile(new File(uri.getPath()));
                            Timber.d(file.getPath());
                            if (file != null) {
                                uploadFile(file);
                            }
                        }
                    }
                });
    }

    private File saveBitmapToFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.PNG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private void uploadFile(File file){
        RequestBody requestBody =RequestBody.create(MediaType.parse("image/png"), file);

        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestBody);

        ProfileWebService webService = WebServiceFactory.createRetrofitService(ProfileWebService.class);

        String cookie = new SharedPrefUtils().getPref(Constants.SHARED_SESSION, context);
        webService.uploadProfilePicture(cookie, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        profileViewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if(jsonObjectResponse.body() != null){
                            Timber.d(jsonObjectResponse.body().toString());
                        }else {
                            Timber.d("body null");
                        }
                        if (jsonObjectResponse.isSuccessful()){
                            if (jsonObjectResponse.body().toString().contains("Avatar Uploaded")) {
                                Toast.makeText(context, "Upload success", Toast.LENGTH_LONG).show();
                                getProfile(cookie);
                            }
                        }else {
                            Toast.makeText(context, "Something went wrong while uploading file", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        profileViewInterface.hideProgress();
                    }
                });
    }
}
