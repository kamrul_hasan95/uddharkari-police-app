package flow.digital.respondentpolice.appModule.profile.view;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.login.view.LoginActivity;
import flow.digital.respondentpolice.appModule.mother.MotherActivity;
import flow.digital.respondentpolice.appModule.profile.presenter.ProfilePresenter;
import flow.digital.respondentpolice.appModule.profile.presenter.ProfileViewInterface;
import flow.digital.respondentpolice.base.BaseFragment;
import flow.digital.respondentpolice.constants.ApiConstants;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.Profile;
import flow.digital.respondentpolice.utils.ProgressBarHandler;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import timber.log.Timber;

public class ProfileFragment extends BaseFragment implements ProfileViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;

    ProgressBarHandler progressBarHandler;
    ProfilePresenter presenter;

    @BindView(R.id.tvFirstName)
    TextView tvFirstName;
    @BindView(R.id.tvLastName)
    TextView tvLastName;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvGender)
    TextView tvGender;
    @BindView(R.id.tvContactNumber)
    TextView tvContactNumber;
    @BindView(R.id.tvDesignation)
    TextView tvDesignation;
    @BindView(R.id.ivProfilePic)
    ImageView ivProfilePic;

    Profile profile;

    public ProfileFragment() {
        //empty constructor for fragment
    }

    public static ProfileFragment newInstance(String title) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarHandler = new ProgressBarHandler(getContext());
        presenter = new ProfilePresenter(getContext(), this);

        presenter.getProfile(new SharedPrefUtils().getPref(Constants.SHARED_SESSION, getContext()));
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }


    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }

    @Override
    public void onProfileFetched(Profile profile) {
        if (profile != null && tvFirstName != null){
            this.profile = profile;
            initView();
        }
    }

    @Override
    public void onLogoutSuccess() {
        getActivity().finish();
        new SharedPrefUtils().clearAllPref(getContext());
        startActivity(new Intent(getContext(), LoginActivity.class));
    }

    @Override
    public void onProfilePictureUploaded() {
        loadPicture();
    }

    void initView(){
        tvFirstName.setText(getResources().getString(R.string.first_name)+": "+profile.respondentFirstName);
        tvLastName.setText(getResources().getString(R.string.last_name)+": "+profile.respondentLastName);
        tvAge.setText(getResources().getString(R.string.age)+": "+profile.respondentAge);
        tvDesignation.setText(getResources().getString(R.string.designation)+" "+profile.respondentDesignation);
        tvContactNumber.setText(getResources().getString(R.string.contact_number)+": "+profile.respondentContactNumber);
        tvGender.setText(getResources().getString(R.string.gender)+" "+profile.respondentGender);

        loadPicture();
    }

    private void loadPicture(){
        GlideUrl glideUrl = new GlideUrl(ApiConstants.BASE_URL+"getRespondentAvatar",
                new LazyHeaders.Builder().addHeader(
                        "Cookie",
                        new SharedPrefUtils().getPref(Constants.SHARED_SESSION, getContext()
                        )
                )
                        .build());

        Timber.d(glideUrl.getHeaders().toString());
        Timber.d(glideUrl.toStringUrl());

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.ic_person_placeholder);

        Glide.with(this)
                .load(glideUrl)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        ivProfilePic.setVisibility(View.VISIBLE);
                        ivProfilePic.setImageResource(R.drawable.ic_person_placeholder);
                        Timber.d("onLoadfailed");
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ivProfilePic.setVisibility(View.VISIBLE);
                        Timber.d("onResourceReady");
                        ivProfilePic.setImageDrawable(resource);
                        return true;
                    }
                })
                .apply(options)
                .into(ivProfilePic);
    }

    @OnClick(R.id.ivProfilePic)
    public void changeProfilePic(){
        presenter.getPicture((MotherActivity)getActivity());
    }

    @OnClick(R.id.btnProfileUpdate)
    public void profielUpdate(){
        Intent intent = new Intent(getContext(), ProfileEditActivity.class);
        intent.putExtra(Constants.EXTRA_DESIGNATION, profile.respondentDesignation);
        startActivity(intent);
    }

    @OnClick(R.id.btnLogout)
    public void logout(){
        presenter.logout();
    }

    @OnClick(R.id.btnResetPassword)
    public void changePassword(){
        startActivity(new Intent(getContext(), ChangePasswordActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getProfile(new SharedPrefUtils().getPref(Constants.SHARED_SESSION, getContext()));
    }
}
