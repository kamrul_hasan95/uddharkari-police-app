package flow.digital.respondentpolice.appModule.login.presenter;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.JsonObject;

import androidx.annotation.NonNull;
import flow.digital.respondentpolice.apiService.AuthWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.sendModel.FcmTokenSendModel;
import flow.digital.respondentpolice.dataModel.sendModel.LoginSendModel;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class LoginPresenter {
    private Context context;
    private LoginViewInterface viewInterface;
    SharedPrefUtils utils;

    public LoginPresenter(Context context, LoginViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
        utils = new SharedPrefUtils();
    }

    public void login(String userName,String password){
        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        LoginSendModel sendModel = new LoginSendModel();
        sendModel.username = userName;
        sendModel.password = password;

        webService.login(sendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        viewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if (jsonObjectResponse.isSuccessful()){
                            utils.putPref(Constants.SHARED_USERNAME, userName, context);
                            utils.putPref(Constants.SHARED_PASSWORD, password, context);
                            utils.putPref(Constants.SHARED_SESSION, jsonObjectResponse.headers().get("set-cookie"), context);
                            utils.putPref(Constants.SHARED_USERID, jsonObjectResponse.body().get("userId").getAsString(), context);

                            saveFcmToken(jsonObjectResponse.headers().get("set-cookie"));
                        }else {
                            viewInterface.onLoginFailed();
                            viewInterface.hideProgress();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveFcmToken(String sessionId) {
        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        FcmTokenSendModel sendModel = new FcmTokenSendModel();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Timber.w("getInstanceId failed" + task.getException());
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();
                Timber.d("firebase" + token);
                sendModel.userToken = token;
                utils.putPref(Constants.SHARE_TOKEN, token, context);

                webService.saveFcmToken(sessionId, sendModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<JsonObject>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Response<JsonObject> jsonObjectResponse) {
                                Timber.d(jsonObjectResponse.toString());
                                if (jsonObjectResponse.isSuccessful()) {
                                    viewInterface.onLoginSuccess();
                                    utils.putPref(Constants.SHARED_SESSION, sessionId, context);
                                } else {
                                    viewInterface.onLoginFailed();
                                    viewInterface.hideProgress();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                onError(e);
                            }

                            @Override
                            public void onComplete() {
                                viewInterface.hideProgress();
                            }
                        });
            }
        });
    }

//    private void saveToken(String sessionId, String userId){
//        Timber.d(userId);
//        Timber.d(sessionId);
//        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(
//                "http://104.192.5.213/getRespondentBeamToken?userId="+userId,
//                new AuthDataGetter() {
//                    @Override
//                    public AuthData getAuthData() {
//                        // Headers and URL query params your auth endpoint needs to
//                        // request a Beams Token for a given user
//                        HashMap<String, String> headers = new HashMap<>();
//                        // for example:
//                        // headers.put("Authorization", sessionToken);
////                        headers.put("Cookie", sessionId);
//                        Timber.d(headers.toString());
//                        HashMap<String, String> queryParams = new HashMap<>();
//                        return new AuthData(
//                                headers,
//                                queryParams
//                        );
//                    }
//                }
//        );
//
//
//        PushNotifications.setUserId(userId, tokenProvider, new BeamsCallback<Void, PusherCallbackError>(){
//            @Override
//            public void onSuccess(Void... values) {
//                Timber.d("PusherBeams: "+"Successfully authenticated with Pusher Beams");
//                viewInterface.onLoginSuccess();
//                viewInterface.hideProgress();
//            }
//
//            @Override
//            public void onFailure(PusherCallbackError error) {
//                Timber.d("PusherBeams: "+"Pusher Beams authentication failed: " + error.getMessage() +"\n"+ error.toString());
//                viewInterface.onLoginFailed();
//                viewInterface.hideProgress();
//            }
//        });
//    }

}
