package flow.digital.respondentpolice.appModule.profile.presenter;

import flow.digital.respondentpolice.base.MvpView;

public interface ProfileEditViewInterface extends MvpView {
    void onProfileUpdateSuccess();
    void onProfileUpdateFailed();
}
