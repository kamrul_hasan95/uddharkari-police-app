package flow.digital.respondentpolice.appModule.splash.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.login.view.LoginActivity;
import flow.digital.respondentpolice.appModule.mother.MotherActivity;
import flow.digital.respondentpolice.appModule.respond.RespondActivity;
import flow.digital.respondentpolice.appModule.splash.presenter.SplashPresenter;
import flow.digital.respondentpolice.appModule.splash.presenter.SplashViewInterface;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import timber.log.Timber;

public class SplashActivity extends BaseActivity implements SplashViewInterface {

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE_AUDIO = 35;

    private SharedPrefUtils sharedPrefUtils;
    private SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefUtils = new SharedPrefUtils();

        Timber.d("timber timber");

        Bundle extras = getIntent().getExtras();

        if (extras != null){
            String lat = extras.getString("latitude");
            String lon = extras.getString("longitude");
            String firstName = extras.getString("victim_first_name");
            String lastName = extras.getString("victim_last_name");
            String emergencyContact = extras.getString("victim_emergency_contact");
            String emergencyContactRelation = extras.getString("victim_emergency_relation");
            String age = extras.getString("victim_age");
            String phone = extras.getString("victim_contact_number");

            Intent intent = new Intent(getApplicationContext(), RespondActivity.class);
            intent.putExtra(Constants.EXTRA_VICTIM_FIRST_NAME, firstName);
            intent.putExtra(Constants.EXTRA_VICTIM_LAST_NAME, lastName);
            intent.putExtra(Constants.EXTRA_VICTIM_AGE, age);
            intent.putExtra(Constants.EXTRA_VICTIM_EMERGENCY_CONTACT_NUMBER, emergencyContact);
            intent.putExtra(Constants.EXTRA_VICTIM_EMERGENCY_RELATION, emergencyContactRelation);
            intent.putExtra(Constants.EXTRA_VICTIM_LAT, lat);
            intent.putExtra(Constants.EXTRA_VICTIM_LON, lon);
            intent.putExtra(Constants.EXTRA_VICTIM_PHONE, phone);

            if(lat != null && lon != null) {
                startActivity(intent);
                finish();
            }else {
                login();
            }
        }else {
            login();
        }
    }

    private void login(){
        presenter = new SplashPresenter(this, this);

        if (checkPermissions()) {
            presenter.login(sharedPrefUtils.getPref(Constants.SHARED_USERNAME, this), sharedPrefUtils.getPref(Constants.SHARED_PASSWORD, this));
        } else {
            requestPermissions();
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }

    private void goToLoginActivity(){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void goToMotherActivity(){
        startActivity(new Intent(this, MotherActivity.class));
        finish();
    }

    @Override
    public void onSuccess() {
        goToMotherActivity();
    }

    @Override
    public void onError() {
        goToLoginActivity();
    }

    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                && PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                && PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        boolean storageRead =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

        boolean storageWrite =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale && storageRead && storageWrite) {
            Timber.i("Displaying permission rationale to provide additional context.");
//            Snackbar.make(
//                    findViewById(R.id.activity_main),
//                    R.string.permission_rationale,
//                    Snackbar.LENGTH_INDEFINITE)
//                    .setAction(R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            // Request permission
//                            ActivityCompat.requestPermissions(MainActivity.this,
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                    REQUEST_PERMISSIONS_REQUEST_CODE);
//                        }
//                    })
//                    .show();
        } else {
            Timber.i( "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Timber.i("onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Timber.i("User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                presenter.login(sharedPrefUtils.getPref(Constants.SHARED_USERNAME, this), sharedPrefUtils.getPref(Constants.SHARED_PASSWORD, this));
            } else {
                // Permission denied.
//                setButtonsState(false);
//                Snackbar.make(
//                        findViewById(R.id.activity_main),
//                        R.string.permission_denied_explanation,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setAction(R.string.settings, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                // Build intent that displays the App settings screen.
//                                Intent intent = new Intent();
//                                intent.setAction(
//                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package",
//                                        BuildConfig.APPLICATION_ID, null);
//                                intent.setData(uri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }
//                        })
//                        .show();
            }
        }
    }
}
