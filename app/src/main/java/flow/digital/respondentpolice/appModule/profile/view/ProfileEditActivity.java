package flow.digital.respondentpolice.appModule.profile.view;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.profile.presenter.ProfileEditPresenter;
import flow.digital.respondentpolice.appModule.profile.presenter.ProfileEditViewInterface;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.ProfileUpdateData;
import flow.digital.respondentpolice.dataModel.sendModel.ProfileUpdateSendModel;
import flow.digital.respondentpolice.utils.ProgressBarHandler;

public class ProfileEditActivity extends BaseActivity implements ProfileEditViewInterface {
    private ProfileEditPresenter presenter;
    private ProgressBarHandler progressBarHandler;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.spnRelation)
    Spinner spnRelation;

    int selectSpnItem;
    List<String> designationArray;
    String designation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setTitle(R.string.profile_update);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            designation = extras.getString(Constants.EXTRA_DESIGNATION);
        }

        presenter = new ProfileEditPresenter(this, this);
        progressBarHandler = new ProgressBarHandler(this);

        initData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void initData(){
        designationArray = Arrays.asList(getResources().getStringArray(R.array.designation_array));
        for (int i = 0 ; i < designationArray.size() ; i ++){
            if (designationArray.get(i).equalsIgnoreCase(designation)) {
                selectSpnItem = i;
            }
        }
        initView();
    }

    private void initView(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_spinner_text, designationArray){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(16);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };

        spnRelation.setAdapter(adapter);


        spnRelation.setSelection(selectSpnItem);

        spnRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectSpnItem = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @OnClick(R.id.btnUpdate)
    public void update(){
        if (validateDesignation()){
            ProfileUpdateData profileUpdateData = new ProfileUpdateData();
            profileUpdateData.designation = designationArray.get(selectSpnItem);

            ProfileUpdateSendModel profileUpdateSendModel = new ProfileUpdateSendModel();
            profileUpdateSendModel.updates = profileUpdateData;

            presenter.udpateProfile(profileUpdateSendModel);
        }
    }

    private boolean validateDesignation(){
        if (designation.equalsIgnoreCase(designationArray.get(selectSpnItem))){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_profile_edit;
    }

    @Override
    public void onProfileUpdateSuccess() {
        finish();
    }

    @Override
    public void onProfileUpdateFailed() {

    }

    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
