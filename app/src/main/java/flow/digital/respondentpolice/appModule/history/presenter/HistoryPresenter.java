package flow.digital.respondentpolice.appModule.history.presenter;

import android.content.Context;

import flow.digital.respondentpolice.apiService.HistoryWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.dataModel.response.HistoryResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class HistoryPresenter {
    private Context context;
    private HistoryViewInterface viewInterface;

    public HistoryPresenter(Context context, HistoryViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void getHistory(String sessionid){
        HistoryWebService webService = WebServiceFactory.createRetrofitService(HistoryWebService.class);

        webService.getVictimHistory(sessionid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<HistoryResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        viewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<HistoryResponse> historyResponseResponse) {
                        Timber.d(historyResponseResponse.toString());
                        if (historyResponseResponse.isSuccessful()){
                            Timber.d(historyResponseResponse.body().toString());
                            viewInterface.onHistoryResponse(historyResponseResponse.body());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        viewInterface.hideProgress();
                    }
                });
    }
}
