package flow.digital.respondentpolice.appModule.registration.presenter;

import android.content.Context;

import com.google.gson.JsonObject;

import flow.digital.respondentpolice.apiService.AuthWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.dataModel.sendModel.RegistrationSendModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

public class RegistrationPresenter {
    private Context context;
    private RegistrationViewInterface viewInterface;

    public RegistrationPresenter(Context context, RegistrationViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void registerUser(String firstName,
                             String lastName,
                             String age,
                             String designation,
                             String contactNumber,
                             String gender,
                             String password,
                             String station){
        AuthWebService webService = WebServiceFactory.createRetrofitService(AuthWebService.class);

        RegistrationSendModel sendModel = new RegistrationSendModel();
        sendModel.age = age;
        sendModel.designation = designation;
        sendModel.firstName = firstName;
        sendModel.lastName = lastName;
        sendModel.gender = gender;
        sendModel.username = contactNumber;
        sendModel.password = password;
        sendModel.contactNumber = contactNumber;
        sendModel.stationName = station;

        Timber.d("xyz"+sendModel.toString());

        webService.registerUser(sendModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        viewInterface.showProgress();
                    }

                    @Override
                    public void onNext(Response<JsonObject> jsonObjectResponse) {
                        Timber.d(jsonObjectResponse.toString());
                        if (jsonObjectResponse.isSuccessful()){
                            viewInterface.onRegistrationSuccess();
                        }else {
                            viewInterface.onRegistrationFailed();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        viewInterface.hideProgress();
                    }
                });
    }
}
