package flow.digital.respondentpolice.appModule.profile.presenter;

import java.io.File;

import flow.digital.respondentpolice.base.MvpView;
import flow.digital.respondentpolice.dataModel.Profile;

public interface ProfileViewInterface extends MvpView {
    void onProfileFetched(Profile profile);
    void onLogoutSuccess();
    void onProfilePictureUploaded();
}
