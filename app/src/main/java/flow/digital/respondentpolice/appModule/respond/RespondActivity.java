package flow.digital.respondentpolice.appModule.respond;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.base.BaseActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.services.LocationUpdateService;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import timber.log.Timber;

public class RespondActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvVictimAge)
    TextView tvVictimAge;
    @BindView(R.id.tvVictimEmergencyContact)
    TextView tvVictimEmergencyContact;
    @BindView(R.id.tvVictimEmergencyContactRelation)
    TextView tvVictimEmergencyContactRelation;
    @BindView(R.id.tvVictimFirstName)
    TextView tvFirstName;
    @BindView(R.id.tvVictimLastName)
    TextView tvLastName;
    @BindView(R.id.helpLayout)
    ConstraintLayout helpLayout;
    @BindView(R.id.tvSocketResponses)
    TextView tvSocketResponses;
    @BindView(R.id.btnRescue)
    Button btnRescue;
    @BindView(R.id.btnCaseCompleted)
    Button btnCaseCompleted;

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String data = intent.getStringExtra(LocationUpdateService.EXTRA_LOCATION);
            if (data != null) {
                Timber.d("respondentActivity xyz: "+data);
                webSocketResponse = data;
                tvSocketResponses.setText(webSocketResponse);
                if(data.contains("Case Closed")){
                    finishAct();
                }
            }

            boolean caseAccepted = intent.getBooleanExtra(LocationUpdateService.EXTRA_CASE_ACCEPTED, false);
            if (caseAccepted){
                btnCaseCompleted.setVisibility(View.VISIBLE);
            }

        }
    }

    private MyReceiver myReceiver;

    String firstname, lastname, age, emergencyContact, relation, latt, longi, victimPhone;
    private String webSocketResponse;

    // A reference to the service used to get location updates.
    private LocationUpdateService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Timber.d("connected");
            LocationUpdateService.LocalBinder binder = (LocationUpdateService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setTitle("Respond for Help");

        Bundle extras = getIntent().getExtras();

        if (extras != null){
            firstname = extras.getString(Constants.EXTRA_VICTIM_FIRST_NAME);
            lastname = extras.getString(Constants.EXTRA_VICTIM_LAST_NAME);
            latt = extras.getString(Constants.EXTRA_VICTIM_LAT);
            longi = extras.getString(Constants.EXTRA_VICTIM_LON);
            age = extras.getString(Constants.EXTRA_VICTIM_AGE);
            emergencyContact = extras.getString(Constants.EXTRA_VICTIM_EMERGENCY_CONTACT_NUMBER);
            relation = extras.getString(Constants.EXTRA_VICTIM_EMERGENCY_RELATION);
            victimPhone = extras.getString(Constants.EXTRA_VICTIM_PHONE);

            myReceiver = new MyReceiver();
            initView();
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_respond;
    }

    @Override
    public void onStart() {
        super.onStart();


        bindService(new Intent(this, LocationUpdateService.class),
                mServiceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdateService.ACTION_BROADCAST));
    }

    @Override
    public void onStop() {
        Timber.d("onStop");
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initView(){
        helpLayout.setVisibility(View.GONE);
        tvFirstName.setText("First name: "+firstname);
        tvLastName.setText("Last name: "+lastname);
        tvVictimAge.setText("Age: "+age);
        tvVictimEmergencyContact.setText("Emergency Contact Number: "+emergencyContact);
        tvVictimEmergencyContactRelation.setText("Emergency Contact relation: "+relation);
    }

    @OnClick(R.id.btnCall)
    public void call(){
        Intent intent=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+victimPhone));
        startActivity(intent);
    }

    @OnClick(R.id.btnVictimLocation)
    public void location(){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+latt+","+longi);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @OnClick(R.id.btnRescue)
    public void rescue(){
        if (victimPhone != null && victimPhone.contains("+88")) {
            btnRescue.setEnabled(false);
            mService.requestLocationUpdates(new SharedPrefUtils().getPref(Constants.SHARED_USERNAME, this), victimPhone);
            helpLayout.setVisibility(View.VISIBLE);
        }
    }

    private void finishAct(){
        finish();
    }

    @OnClick(R.id.btnCaseCompleted)
    public void closeCase(){
        mService.closeCase();
    }
}