package flow.digital.respondentpolice.appModule.history.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.history.adapter.AdapterRvHistory;
import flow.digital.respondentpolice.appModule.history.presenter.HistoryPresenter;
import flow.digital.respondentpolice.appModule.history.presenter.HistoryViewInterface;
import flow.digital.respondentpolice.base.BaseFragment;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.response.HistoryResponse;
import flow.digital.respondentpolice.utils.ProgressBarHandler;
import flow.digital.respondentpolice.utils.SharedPrefUtils;

public class HistoryFragment extends BaseFragment implements HistoryViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;

    ProgressBarHandler progressBarHandler;
    HistoryPresenter presenter;

    @BindView(R.id.tvSessionAct)
    TextView tvSessionAct;
    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;

    HistoryResponse historyResponse;

    public HistoryFragment() {
        //empty constructor for fragment
    }

    public static HistoryFragment newInstance(String title) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarHandler = new ProgressBarHandler(getContext());
        presenter = new HistoryPresenter(getContext(), this);

        presenter.getHistory(new SharedPrefUtils().getPref(Constants.SHARED_SESSION, getContext()));
    }

    private void initView(){
        rvHistory.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvHistory.setHasFixedSize(true);
        rvHistory.setItemAnimator(new DefaultItemAnimator());

        if (historyResponse.histories.size() > 0) {
            tvSessionAct.setText(getResources().getString(R.string.text_last_session_activated) + " " +historyResponse.histories.get(0).lastSessionActivated);
        }

        if (historyResponse != null && historyResponse.histories !=null) {
            AdapterRvHistory adapter = new AdapterRvHistory(getContext(), historyResponse.cases);

            rvHistory.setAdapter(adapter);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_history;
    }


    @Override
    public void showProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }

    @Override
    public void onHistoryResponse(HistoryResponse response) {
        if (rvHistory != null){
            this.historyResponse = response;
            initView();
        }
    }
}
