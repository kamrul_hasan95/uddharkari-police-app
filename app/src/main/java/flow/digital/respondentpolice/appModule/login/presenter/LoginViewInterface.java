package flow.digital.respondentpolice.appModule.login.presenter;

import flow.digital.respondentpolice.base.MvpView;

public interface LoginViewInterface extends MvpView {
    void onLoginSuccess();
    void onLoginFailed();
}
