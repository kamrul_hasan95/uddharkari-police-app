package flow.digital.respondentpolice.constants;


public interface ApiConstants {

    //API BASE url
    String BASE_URL = "http://104.192.5.213/";
    String HEADER_NAME_CONTENT = "Content-Type";
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_MULTIPART = "multipart/form-data";
}
