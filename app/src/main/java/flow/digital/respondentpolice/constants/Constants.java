package flow.digital.respondentpolice.constants;


public interface Constants {
    String SHARED_PREF = "uddharkariPolicePref";

    String SHARED_USERNAME = "username_shared";
    String SHARED_PASSWORD = "password_shared";
    String SHARED_SESSION = "session_id";
    String SHARED_USERID = "user_id";
    String SHARE_TOKEN = "firebase_token";

    String PACKAGE_NAME = "flow.digital.sultana";
    String CHANNEL_ID = "flow_notification_channel_loc";
    int NOTIFICATION_ID = 341;
    long LOC_UPDATE_INTERVAL = 60000;

    String EXTRA_DESIGNATION = "extra_designation";

    String EXTRA_VICTIM_FIRST_NAME = "victim_first_name";
    String EXTRA_VICTIM_LAST_NAME = "victim_last_name";
    String EXTRA_VICTIM_AGE = "victim_age";
    String EXTRA_VICTIM_EMERGENCY_CONTACT_NUMBER = "victim_emergency_contact_number";
    String EXTRA_VICTIM_EMERGENCY_RELATION = "victim_relation";
    String EXTRA_VICTIM_LAT = "victim_lat";
    String EXTRA_VICTIM_LON = "victim_lon";
    String EXTRA_VICTIM_PHONE = "victim_phone";
}