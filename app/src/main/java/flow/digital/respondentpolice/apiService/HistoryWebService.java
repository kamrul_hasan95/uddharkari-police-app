package flow.digital.respondentpolice.apiService;


import flow.digital.respondentpolice.dataModel.response.HistoryResponse;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface HistoryWebService {
    @POST("getRespondentHistories")
    Observable<Response<HistoryResponse>> getVictimHistory(
            @Header("Cookie") String cookie
    );

//    @POST("getVictimHistories")
//    Observable
}
