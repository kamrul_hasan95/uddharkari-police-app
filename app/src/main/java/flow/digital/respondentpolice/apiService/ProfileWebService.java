package flow.digital.respondentpolice.apiService;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.File;

import flow.digital.respondentpolice.dataModel.response.ProfileResponse;
import flow.digital.respondentpolice.dataModel.sendModel.ChangePasswordSendModel;
import flow.digital.respondentpolice.dataModel.sendModel.ProfileUpdateSendModel;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ProfileWebService {
    @POST("getRespondentProfile")
    Observable<Response<ProfileResponse>> getProfile(
            @Header("Cookie") String cookie
    );

    @FormUrlEncoded
    @POST("updateRespondentProfile")
    Observable<Response<JsonObject>> updateProfile(
            @Header("Cookie") String cookie,
            @Field("updates") String update
            );

    @POST("updateRespondentPassword")
    Observable<Response<JsonObject>> changePassword(
            @Header("Cookie") String cookie,
            @Body ChangePasswordSendModel body
    );

    @Multipart
    @POST("uploadRespondentAvatar")
    Observable<Response<JsonObject>> uploadProfilePicture(
            @Header("Cookie") String cookie,
            @Part MultipartBody.Part file
            );

//    @Multipart
    @GET("getRespondentAvatar")
    Observable<Response<File>> getProfilePicture(
            @Header("Cookie") String cookie
    );
}
