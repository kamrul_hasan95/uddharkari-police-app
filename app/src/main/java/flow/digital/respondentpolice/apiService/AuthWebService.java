package flow.digital.respondentpolice.apiService;

import com.google.gson.JsonObject;

import flow.digital.respondentpolice.dataModel.sendModel.FcmTokenSendModel;
import flow.digital.respondentpolice.dataModel.sendModel.LoginSendModel;
import flow.digital.respondentpolice.dataModel.sendModel.RegistrationSendModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AuthWebService {
    @POST("respondentLogin")
    Observable<Response<JsonObject>> login(
            @Body LoginSendModel sendModel
            );

    @POST("registerRespondent")
    Observable<Response<JsonObject>> registerUser(
            @Body RegistrationSendModel sendModel
            );

    @POST("respondentLogout")
    Observable<Response<JsonObject>> logout(
            @Header("Cookie") String cookie
    );

    @POST("registerRespondentToken")
    Observable<Response<JsonObject>> saveFcmToken(
            @Header("Cookie") String cookie,
            @Body FcmTokenSendModel sendModel
            );
}
