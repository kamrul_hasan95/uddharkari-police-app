package flow.digital.respondentpolice.apiService;

import com.google.gson.JsonObject;

import flow.digital.respondentpolice.dataModel.sendModel.LocationSendModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LocationWebService {
    @POST("updateRespondentLocation")
    Observable<Response<JsonObject>> updateLocation(
            @Body LocationSendModel sendModel
            );
}
