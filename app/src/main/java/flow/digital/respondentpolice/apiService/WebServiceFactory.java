package flow.digital.respondentpolice.apiService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import flow.digital.respondentpolice.base.BaseApplication;
import flow.digital.respondentpolice.constants.ApiConstants;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class WebServiceFactory {
    /**
     * Creates a retrofit service from an arbitrary class (clazz)
     *
     * @return retrofit service with defined endpoint/base-url
     */

    private static final String CACHE_CONTROL = "Cache-Control";

    public static <T> T createRetrofitService(Class<T> service) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttpClient())
                .build();

        return retrofit.create(service);
    }

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .method(original.method(), original.body())
                    .addHeader(ApiConstants.HEADER_NAME_CONTENT, ApiConstants.CONTENT_TYPE_JSON)
                    .addHeader(ApiConstants.HEADER_NAME_CONTENT, ApiConstants.CONTENT_TYPE_MULTIPART)
                    .build();

            return chain.proceed(request);

        });



        httpClient.readTimeout(100, TimeUnit.SECONDS)
                .connectTimeout(100, TimeUnit.SECONDS)
                .addInterceptor(mLoggingInterceptor);


        return httpClient.build();
    }

    private static final Interceptor mLoggingInterceptor = chain -> {
        Request request = chain.request();
        long t1 = System.nanoTime();
        Timber.i(String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));
        Response response = chain.proceed(request);
        long t2 = System.nanoTime();
        Timber.i(String.format(Locale.getDefault(), "Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));
        return response;
    };
}
