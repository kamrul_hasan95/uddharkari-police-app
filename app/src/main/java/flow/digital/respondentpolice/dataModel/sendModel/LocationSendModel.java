package flow.digital.respondentpolice.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationSendModel {
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("is_active")
    @Expose
    public boolean isActive;

    public LocationSendModel(String latitude, String longitude, boolean isActive) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.isActive = isActive;
    }
}
