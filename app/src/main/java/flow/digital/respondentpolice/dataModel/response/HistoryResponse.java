package flow.digital.respondentpolice.dataModel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import flow.digital.respondentpolice.dataModel.Case;
import flow.digital.respondentpolice.dataModel.History;


public class HistoryResponse {
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("histories")
    @Expose
    public List<History> histories = null;
    @SerializedName("cases")
    @Expose
    public List<Case> cases = null;

    @Override
    public String toString() {
        return "HistoryResponse{" +
                "status=" + status +
                ", histories=" + histories +
                ", cases=" + cases +
                '}';
    }
}
