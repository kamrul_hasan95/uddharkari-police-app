package flow.digital.respondentpolice.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class History {
    @SerializedName("last_session_activated")
    @Expose
    public String lastSessionActivated;

    @Override
    public String toString() {
        return "History{" +
                "lastSessionActivated='" + lastSessionActivated + '\'' +
                '}';
    }
}
