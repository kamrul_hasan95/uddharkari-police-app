package flow.digital.respondentpolice.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Case {
    @SerializedName("filed_at")
    @Expose
    public String filedAt;
    @SerializedName("crime_location")
    @Expose
    public String crimeLocation;
    @SerializedName("city_reported")
    @Expose
    public String cityReported;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("victim_rating")
    @Expose
    public String victimRating;
    @SerializedName("victim_first_name")
    @Expose
    public String victimFirstName;
    @SerializedName("victim_last_name")
    @Expose
    public String victimLastName;

    @Override
    public String toString() {
        return "Case{" +
                "filedAt='" + filedAt + '\'' +
                ", crimeLocation='" + crimeLocation + '\'' +
                ", cityReported='" + cityReported + '\'' +
                ", district='" + district + '\'' +
                ", victimRating='" + victimRating + '\'' +
                ", victimFirstName='" + victimFirstName + '\'' +
                ", victimLastName='" + victimLastName + '\'' +
                '}';
    }
}
