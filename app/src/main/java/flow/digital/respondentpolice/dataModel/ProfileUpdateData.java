package flow.digital.respondentpolice.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileUpdateData {
    @SerializedName("designation")
    @Expose
    public String designation;
}
