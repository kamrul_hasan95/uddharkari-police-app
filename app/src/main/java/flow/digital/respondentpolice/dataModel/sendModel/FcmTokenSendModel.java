package flow.digital.respondentpolice.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FcmTokenSendModel {
    @SerializedName("userToken")
    @Expose
    public String userToken;
}
