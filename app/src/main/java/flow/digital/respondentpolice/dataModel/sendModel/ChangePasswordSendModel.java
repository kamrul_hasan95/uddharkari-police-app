package flow.digital.respondentpolice.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordSendModel {
    @SerializedName("new_password")
    @Expose
    public String newPassword;
}
