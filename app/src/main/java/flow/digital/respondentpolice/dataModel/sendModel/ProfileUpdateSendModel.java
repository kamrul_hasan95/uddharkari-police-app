package flow.digital.respondentpolice.dataModel.sendModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import flow.digital.respondentpolice.dataModel.ProfileUpdateData;

public class ProfileUpdateSendModel {
    @SerializedName("updates")
    @Expose
    public ProfileUpdateData updates;
}
