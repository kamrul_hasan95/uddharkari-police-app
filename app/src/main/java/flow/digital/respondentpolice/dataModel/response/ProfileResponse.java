package flow.digital.respondentpolice.dataModel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import flow.digital.respondentpolice.dataModel.Profile;


public class ProfileResponse {
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("profile")
    @Expose
    public Profile profile;
}
