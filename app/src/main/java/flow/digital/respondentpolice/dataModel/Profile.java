package flow.digital.respondentpolice.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {
    @SerializedName("respondent_first_name")
    @Expose
    public String respondentFirstName;
    @SerializedName("respondent_last_name")
    @Expose
    public String respondentLastName;
    @SerializedName("respondent_age")
    @Expose
    public Integer respondentAge;
    @SerializedName("respondent_gender")
    @Expose
    public String respondentGender;
    @SerializedName("respondent_contact_number")
    @Expose
    public String respondentContactNumber;
    @SerializedName("respondent_designation")
    @Expose
    public String respondentDesignation;
}
