package flow.digital.respondentpolice.services;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.jetbrains.annotations.NotNull;

import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.appModule.respond.RespondActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import timber.log.Timber;

public class NotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null) {
            Timber.d(remoteMessage.toString());
            Timber.d(remoteMessage.getNotification().getSound());
//            Timber.d(remoteMessage.getNotification().getChannelId());

            Timber.d(remoteMessage.getData().toString());
            String lat = remoteMessage.getData().get("latitude");
            String lon = remoteMessage.getData().get("longitude");
            String firstName = remoteMessage.getData().get("victim_first_name");
            String lastName = remoteMessage.getData().get("victim_last_name");
            String emergencyContact = remoteMessage.getData().get("victim_emergency_contact");
            String emergencyContactRelation = remoteMessage.getData().get("victim_emergency_relation");
            String age = remoteMessage.getData().get("victim_age");
            String phone = remoteMessage.getData().get("victim_contact_number");
            Timber.d("victimphone: "+phone);

            Intent intent = new Intent(getApplicationContext(), RespondActivity.class);
            intent.putExtra(Constants.EXTRA_VICTIM_FIRST_NAME, firstName);
            intent.putExtra(Constants.EXTRA_VICTIM_LAST_NAME, lastName);
            intent.putExtra(Constants.EXTRA_VICTIM_AGE, age);
            intent.putExtra(Constants.EXTRA_VICTIM_EMERGENCY_CONTACT_NUMBER, emergencyContact);
            intent.putExtra(Constants.EXTRA_VICTIM_EMERGENCY_RELATION, emergencyContactRelation);
            intent.putExtra(Constants.EXTRA_VICTIM_LAT, lat);
            intent.putExtra(Constants.EXTRA_VICTIM_LON, lon);
            intent.putExtra(Constants.EXTRA_VICTIM_PHONE, phone);
            startActivity(intent);
        }else {
            Timber.d("null");
        }
    }

    @Override
    public void onNewToken(String token){
        new SharedPrefUtils().putPref(Constants.SHARE_TOKEN, token, getApplicationContext());
    }
}
