package flow.digital.respondentpolice.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.apiService.LocationWebService;
import flow.digital.respondentpolice.apiService.WebServiceFactory;
import flow.digital.respondentpolice.appModule.splash.view.SplashActivity;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.dataModel.sendModel.LocationSendModel;
import flow.digital.respondentpolice.utils.AppUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import timber.log.Timber;

import static flow.digital.respondentpolice.constants.Constants.NOTIFICATION_ID;


public class LocationUpdateService extends Service {
    private String phoneNumber;
    private String victimPhoneNumber;

    String fileName;
    MediaRecorder mediaRecorder;

    private static final String TAG = LocationUpdateService.class.getSimpleName();

    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = Constants.PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = Constants.PACKAGE_NAME + ".location";

    public static final String EXTRA_CASE_ACCEPTED = "extra_case_accpeted";

    private static final String EXTRA_STARTED_FROM_NOTIFICATION = Constants.PACKAGE_NAME +
            ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();

    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            Constants.LOC_UPDATE_INTERVAL / 2;

    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    private LocationRequest mLocationRequest;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    private Location mLocation;

    private boolean caseAccepted = false;

    private final class EchoWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, okhttp3.Response response) {
            Timber.d(response.toString());
            webSocket.send("{\"path\":\"/openRespondentSocket\",\"number\":\"" + phoneNumber + "\"}");
            Timber.d("{\"path\":\"/openRespondentSocket\",\"number\":\"" + phoneNumber + "\"}");
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Timber.d("Receiving : " + text);


            Intent intent1 = new Intent(ACTION_BROADCAST);
            intent1.putExtra(EXTRA_LOCATION, text);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent1);

            if (text.contains("Respondent Registered")){
                if (mLocation!=null){
                    webSocket.send("{\"path\":\"/respondentCaseAccept\",\"number\":\"" + phoneNumber + "\", \"latitude\":" + mLocation.getLatitude() + ", \"longitude\": " + mLocation.getLongitude()
                            + ", \"victim_number\" : \"" + victimPhoneNumber + "\"}");
                    Timber.d("{\"path\":\"/respondentCaseAccept\",\"number\":\"" + phoneNumber + "\", \"latitude\":" + mLocation.getLatitude() + ", \"longitude\": " + mLocation.getLongitude()
                            + ", \"victim_number\" : \"" + victimPhoneNumber + "\"}");
                }
            }else if (text.contains("Case Accepted")){
                if (mLocation!=null) {
                    caseAccepted = true;
                    webSocket.send("{\"path\":\"/updateRespondentLocation\",\"number\":\""+phoneNumber+"\",\"latitude\": \""+mLocation.getLatitude()+"\",\"longitude\": "+mLocation.getLongitude()+"}");
                    Timber.d("{\"path\":\"/updateRespondentLocation\",\"number\":\""+phoneNumber+"\",\"latitude\": \""+mLocation.getLatitude()+"\",\"longitude\": "+mLocation.getLongitude()+"}");

                    Intent intent = new Intent(ACTION_BROADCAST);
                    intent.putExtra(EXTRA_CASE_ACCEPTED, true);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            }else if (text.contains("Case Closed")){
                finish();
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            Timber.d("Receiving bytes : " + bytes.hex());
//            output("Receiving bytes : " + bytes.hex());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            Timber.d("Closing : " + code + " / " + reason);
//            output("Closing : " + code + " / " + reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            Timber.d("Error : " + t.getMessage());
//            output("Error : " + t.getMessage());
        }
    }

    private WebSocket ws;

    public LocationUpdateService() {
    }

    private void finish(){
        stopSelf();
    }

    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false);

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Timber.i("Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && AppUtils.requestingLocationUpdates(this)) {
            Timber.i("Starting foreground service");
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        LocationUpdatesService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, new Intent(this,
                        LocationUpdateService.class));
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
//            startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates(String phoneNumber, String victimPhoneNumber) {
        this.phoneNumber = phoneNumber;
        this.victimPhoneNumber = victimPhoneNumber;

        Timber.i("Requesting location updates");

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url("ws://104.192.5.213/respondent").build();

        EchoWebSocketListener listener = new EchoWebSocketListener();

        ws = client.newWebSocket(request, listener);

        AppUtils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            AppUtils.setRequestingLocationUpdates(this, false);
            Timber.i("Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Timber.i("Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            AppUtils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            AppUtils.setRequestingLocationUpdates(this, true);
            Timber.i("Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_channel_name);
            String description = getString(R.string.notification_channel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Sets whether notifications posted to this channel should display notification lights
            channel.enableLights(true);
            // Sets whether notification posted to this channel should vibrate.
            channel.enableVibration(true);
            // Sets the notification light color for notifications posted to this channel
            channel.setLightColor(Color.GREEN);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdateService.class);

        CharSequence text = AppUtils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SplashActivity.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentText(text)
                .setContentTitle(AppUtils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                            } else {
                                Timber.w(TAG, "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Timber.e("Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        Timber.i("New location: " + location);

        mLocation = location;


        if (caseAccepted) {
            ws.send("{\"path\":\"/updateRespondentLocation\",\"number\":\""+phoneNumber+"\",\"latitude\": \""+mLocation.getLatitude()+"\",\"longitude\": "+mLocation.getLongitude()+"}");
            Timber.d("{\"path\":\"/updateRespondentLocation\",\"number\":\""+phoneNumber+"\",\"latitude\": \""+mLocation.getLatitude()+"\",\"longitude\": "+mLocation.getLongitude()+"}");
        }

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
//            updateLocation(location);
            mNotificationManager.notify(NOTIFICATION_ID, getNotification());
        }
    }

    public void closeCase(){
        ws.send("{\"path\":\"/closeCase\",\"number\":\""+phoneNumber+"\", \"victim_number\" : \""+victimPhoneNumber+"\"}");
        Timber.d("{\"path\":\"/closeCase\",\"number\":\""+phoneNumber+"\", \"victim_number\" : \""+victimPhoneNumber+"\"}");
        removeLocationUpdates();
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.LOC_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public LocationUpdateService getService() {
            return LocationUpdateService.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }
}
