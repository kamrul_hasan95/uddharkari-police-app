package flow.digital.respondentpolice.base;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioAttributes;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;

import androidx.multidex.MultiDex;
import flow.digital.respondentpolice.BuildConfig;
import flow.digital.respondentpolice.R;
import flow.digital.respondentpolice.constants.Constants;
import flow.digital.respondentpolice.utils.SharedPrefUtils;
import timber.log.Timber;

public class BaseApplication extends Application {
    private static BaseApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        //configure timber for logging
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

//        PushNotifications.start(getApplicationContext(), "d3544454-1a5f-419a-89ae-2bcc66ee2cec");

//        PushNotificationsInstance instance = PushNotifications.start(getApplicationContext(), "d3544454-1a5f-419a-89ae-2bcc66ee2cec");

        createNotificationChannel();

        if (!new SharedPrefUtils().getPref(Constants.SHARED_SESSION, this).isEmpty()){
//            saveToken();
        }
    }
    private void createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.loud_siren);
            String channelId = this.getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId,   "for getting rescue notification", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("for getting rescue notification");
            channel.setSound(sound, attributes);
            NotificationManager notificationManager = (NotificationManager)getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static BaseApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
