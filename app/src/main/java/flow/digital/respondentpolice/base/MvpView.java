package flow.digital.respondentpolice.base;

public interface MvpView {
    void showProgress();

    void hideProgress();

    void onError(Throwable t);
}
